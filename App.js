import React from 'react';
import {
  View,
  Text,
  SafeAreaView,
  ScrollView,
  useColorScheme,
  StyleSheet,
} from 'react-native';
import {Colors} from 'react-native/Libraries/NewAppScreen';
import {Provider} from 'react-redux';
import {Store, Persistor} from './src/store';
import Todos from './src/container/Todos';
import {PersistGate} from 'redux-persist/integration/react';

const styles = StyleSheet.create({
  root: {
    backgroundColor: '#263238',
    flex: 1,
    flexDirection: 'column',
  },
});

const App = () => {
  // const isDarkMode = useColorScheme() === 'dark';

  // const backgroundStyle = {
  //   backgroundColor: isDarkMode ? Colors.darker : Colors.lighter,
  // };

  const backgroundStyle = {
    backgroundColor: '#FFFFF',
  };

  return (
    <Provider store={Store}>
      <PersistGate loading={null} persistor={Persistor}>
        <SafeAreaView style={styles.root}>
          {/* <ScrollView contentInsetAdjustmentBehavior="automatic"> */}
          <Todos />
          {/* </ScrollView> */}
        </SafeAreaView>
      </PersistGate>
    </Provider>
  );
};

export default App;
