import {
  View,
  TouchableOpacity,
  Text,
  TextInput,
  FlatList,
  Alert,
} from 'react-native';
import React, {useState, useEffect} from 'react';
import Icon from 'react-native-vector-icons/Feather';
import styles from './styles';
import {useSelector, useDispatch} from 'react-redux';
import {
  getTodos,
  delTodos,
  postTodos,
  getDetailTodos,
  patchTodos,
  setLoading,
} from '../../actions';
import axios from 'axios';

const Todos = () => {
  const [title, setTitle] = useState('');
  const [description, setDescription] = useState('');
  const [selectedStatus, setselectedStatus] = useState('ON_PROGRESS');
  const [id, setId] = useState(0);

  const data = useSelector(state => state.appData.todos);
  const isLoading = useSelector(state => state.appData.isLoading);
  const detailData = useSelector(state => state.appData.detailTodos);

  const dispatch = useDispatch();
  const setLoadingState = loading => dispatch(setLoading(loading));
  const getTodoData = () => dispatch(getTodos());
  const addTodoData = payload => dispatch(postTodos(payload));
  const updateTodosData = (id, payload) => dispatch(patchTodos(id, payload));
  const delTodoData = todoId => dispatch(delTodos(todoId));

  useEffect(() => {
    getTodoData();
  }, []);

  const cekSave = () => {
    if (id != 0) {
      const payload = {
        title: title,
        description: description,
        status: selectedStatus,
      };
      setLoadingState(true);
      updateTodosData(id, payload);
      // getTodoData();
      cancel();
    } else {
      const payload = {
        title: title,
        description: description,
        status: selectedStatus,
      };
      setLoadingState(true);
      addTodoData(payload);
      // getTodoData();
      cancel();
    }
  };

  const delData = todoId => {
    delTodoData(todoId);
    // getTodoData();
  };

  const cancel = () => {
    setTitle('');
    setDescription('');
    setselectedStatus('ON_PROGRESS');
    setId(0);
  };

  const header = (
    <>
      <View style={styles.headingContainer}>
        <Text style={styles.heading}>Redux Todo List</Text>
      </View>

      <View style={styles.inputContainer}>
        <TextInput
          style={styles.input}
          onChangeText={setTitle}
          value={title}
          placeholder="Title"
          placeholderTextColor="white"
        />
        <TextInput
          style={styles.input}
          onChangeText={setDescription}
          value={description}
          placeholder="Description"
          placeholderTextColor="white"
        />
      </View>
      <View style={styles.statusesContainer}>
        <TouchableOpacity
          onPress={() => setselectedStatus('ON_PROGRESS')}
          style={[
            styles.statusButton,
            selectedStatus === 'ON_PROGRESS' && styles.statusButtonSelected,
          ]}>
          <Text
            style={[
              styles.statusButtonText,
              selectedStatus === 'ON_PROGRESS' &&
                styles.statusButtonTextSelected,
            ]}>
            Progress
          </Text>
        </TouchableOpacity>

        <TouchableOpacity
          onPress={() => setselectedStatus('DONE')}
          style={[
            styles.statusButton,
            selectedStatus === 'DONE' && styles.statusButtonSelected,
          ]}>
          <Text
            style={[
              styles.statusButtonText,
              selectedStatus === 'DONE' && styles.statusButtonTextSelected,
            ]}>
            Done
          </Text>
        </TouchableOpacity>
      </View>
      <View style={{flexDirection: 'row', marginLeft: 'auto', marginTop: 20}}>
        <TouchableOpacity style={styles.saveButon} onPress={() => cekSave()}>
          <Text style={styles.buttonText}>Save</Text>
          <Icon name="save" size={20} color="white" />
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.cancelButton}
          onPress={() => {
            cancel();
          }}>
          <Text style={styles.buttonText}>Cancel</Text>
        </TouchableOpacity>
      </View>

      <View style={styles.content}>
        <Text style={styles.todoDateText}>Monday</Text>
      </View>
    </>
  );

  return (
    <View style={styles.root}>
      <FlatList
        data={data}
        ListHeaderComponent={header}
        onRefresh={() => getTodoData()}
        refreshing={isLoading}
        renderItem={({item, index}) => (
          <View style={styles.cardListContainer} key={index}>
            <View style={styles.todoCard}>
              <View style={styles.todoTitleContainer}>
                <View style={styles.todoActionContainer}>
                  <Text style={styles.todoTitle}>{item.title}</Text>
                  <TouchableOpacity style={styles.doneBadge}>
                    <Text style={styles.doneBadgeText}>{item.status}</Text>
                  </TouchableOpacity>
                </View>

                <View style={styles.todoActionContainer}>
                  <TouchableOpacity
                    style={styles.editButton}
                    onPress={() => {
                      setId(item.id);
                      setTitle(item.title);
                      setDescription(item.title);
                      setselectedStatus(item.status);
                    }}>
                    <Icon name="edit" size={20} color="white" />
                  </TouchableOpacity>
                  <TouchableOpacity onPress={() => delData(item.id)}>
                    <Icon name="trash-2" size={20} color="white" />
                  </TouchableOpacity>
                </View>
              </View>
              <View>
                <Text style={styles.todoDescription}>{item.title}</Text>
              </View>
            </View>
          </View>
        )}
      />
    </View>
  );
};

export default Todos;
