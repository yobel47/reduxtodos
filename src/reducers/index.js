import {
  ADD_TODO_SUCCESS,
  DELETE_TODO_SUCCESS,
  FETCH_TODO_SUCCESS,
  UPDATE_TODO_SUCCESS,
  SET_LOADING,
} from '../types';

const initialState = {
  todos: [''],
  isLoading: false,
};

const TodoReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_TODO_SUCCESS:
      return {
        ...state,
        todos: action.payload,
        isLoading: false,
      };
    case SET_LOADING:
      return {
        ...state,
        isLoading: action.isLoading,
      };

    case ADD_TODO_SUCCESS:
      return {
        ...state,
        isLoading: true,
      };
    case DELETE_TODO_SUCCESS:
      return {
        ...state,
        // isLoading: false,
        isLoading: true,
      };
    case UPDATE_TODO_SUCCESS:
      return {
        ...state,
        isLoading: true,
        // detailTodos: action.payload,
      };

    default:
      return state;
  }
};

export default TodoReducer;
