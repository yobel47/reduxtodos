import axios from 'axios';
import {
  ADD_TODO_SUCCESS,
  DELETE_TODO_SUCCESS,
  FETCH_TODO_SUCCESS,
  UPDATE_TODO_SUCCESS,
  SET_LOADING,
} from '../types';
import {Alert} from 'react-native';

export const setLoading = loading => ({
  type: SET_LOADING,
  isLoading: loading,
});

export const saveTodos = data => ({
  type: FETCH_TODO_SUCCESS,
  payload: data,
});

//FUNCTION GET DATA
export const getTodos = () => {
  return async dispatch => {
    dispatch(setLoading(true));
    await axios
      .get('http://code.aldipee.com/api/v1/todos')
      .then(res => {
        if (res.data.results.length > 0) {
          dispatch(saveTodos(res.data.results));
          console.log('ini get');
        }
      })
      .catch(err => {
        console.log(err);
      });
  };
};

export const removeTodos = () => ({
  type: DELETE_TODO_SUCCESS,
});

//FUNCTION DELETE DATA
export const delTodos = todoId => {
  return async dispatch => {
    dispatch(setLoading(true));
    await axios
      .delete('http://code.aldipee.com/api/v1/todos/' + todoId)
      .then(res => {
        console.log('del success');
        Alert.alert('Success', 'Data has been deleted');
        dispatch(removeTodos());
        dispatch(getTodos());
      })
      .catch(err => {
        console.log(err);
      });
  };
};

export const addTodos = () => ({
  type: ADD_TODO_SUCCESS,
});

//FUNCTION ADD DATA
export const postTodos = payload => {
  return async dispatch => {
    dispatch(setLoading(true));
    await axios
      .post('http://code.aldipee.com/api/v1/todos/', payload)
      .then(res => {
        console.log('add success');
        Alert.alert('Success', 'Data has been added');
        dispatch(addTodos());
        dispatch(getTodos());
      })
      .catch(err => {
        console.log(err);
      });
  };
};

export const updateTodos = () => ({
  type: UPDATE_TODO_SUCCESS,
});

//FUNCTION UPDATE DATA ID
export const patchTodos = (id, payload) => {
  return async dispatch => {
    dispatch(setLoading(true));
    await axios
      .patch('http://code.aldipee.com/api/v1/todos/' + id, payload)
      .then(res => {
        console.log('update success');
        Alert.alert('Success', 'Data has been updated');
        dispatch(updateTodos());
        dispatch(getTodos());
      })
      .catch(err => {
        console.log(err);
      });
  };
};
